import os
import glob
import subprocess
import argparse


def extract_tractogram(lesion_mask, output_folder, dependencies_directory='./dependencies', overwrite=False) :

    """
    First step of disconnectome analysis :
    Uses DSIStudio to extract the disconnectome and its connectivity matrix. This is done in two calls of dsi_studio command line :
    firstly, we isolate the streamlines of the template whole brain tractogram that pass though the lesion mask,
    secondly, we extract the connectivity matrix characterizing these streamlines, using a predefined parcellation atlas

    Args :
        lesion_mask : path to the binary lesion segmentation mask
        output_folder : path to the output_folder where the results will be saved
        dependencies_directory : path to the dependencies directory 
        overwrite : if True will overwrite the results if they already exist. If False will not run
    """

    docker_image_dsi = 'manishka/dsi-studio-docker:v19.10'
    docker_run = 'docker run -u $(id -u):$(id -g) --rm -e DISPLAY=' + os.environ['DISPLAY'] + ' -v /tmp/.X11-unix/:/tmp/.X11-unix'
    
    # path to the tractogram of affected streamlines in trk format
    output_trk = os.path.join(output_folder,'connectivity_loss.trk.gz')
    # path to the connectivity matrix of the affected streamlines
    output_cm = os.path.join(output_folder,'connectivity_loss.mat')

    # checks if the lesion mask exists in MNI
    try :
        lesion_roi = glob.glob(lesion_mask)[0]
    except:
        print('No lesion mask or multiple were found')
        return

    # definition of volumes to be mounted for docker image
    vol = ' -v ' + os.path.abspath(dependencies_directory) + ':/dependencies/ -v ' +\
        os.path.split(lesion_roi)[0] + ':/input/ -v ' +\
        output_folder + ':/output/ '


    docker_base_command = docker_run + vol + docker_image_dsi + ' ./opt/dsistudio/dsi_studio '

    # if streamlines passing through lesions were not isolated yet
    if not os.path.isfile(output_trk) or overwrite :

        # extract affected streamlines
        # define output
        os.makedirs( output_folder, exist_ok=True)
        # command line options to extract streamlines passing through lesion mask from whole brain tractogram
        docker_command =  docker_base_command +\
            '--action=ana --source=/dependencies/HCP842_1mm.fib.gz' +\
            ' --tract=/dependencies/whole_brain.trk.gz' +\
            ' --roi=/input/' + os.path.split(lesion_roi)[1] +\
            ' --output=/output/connectivity_loss.trk.gz'

        subprocess.check_output(docker_command,stderr=subprocess.STDOUT,shell= True)


    # if the connectivity matrix wasn't already extracted
    if not os.path.isfile(output_cm) or overwrite:

        # extract connectivity of affected streamlines using the Brainnetome atlas
        # command line options to extract connectivity of affected streamlines
        docker_command = docker_base_command + \
            ' --action=ana --source=/dependencies/HCP842_1mm.fib.gz'+ \
            ' --connectivity="/dependencies/BN_Atlas_274_combined.nii.gz' + \
            '" --connectivity_value=count --connectivity_type=pass' + \
            ' --tract=' + output_trk.replace(output_folder, '/output/') +\
            ' --output=/output/connectivity_loss.mat'
        
        subprocess.check_output(docker_command,stderr=subprocess.STDOUT,shell= True)




if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description="Extracting streamlines passing through lesions")

    PARSER.add_argument('-d', '--dependencies_directory', dest='dependencies_directory', action='store', required=False, type=str, default ='./dependencies',
                        help='Path to the dependencies directory. Default is ./dependencies ')

    PARSER.add_argument('-o', '--output_directory', dest='output_folder', action='store', required=True, type=str,
                        help='Output folder of disconnectome')

    PARSER.add_argument('-f', '--force', dest='overwrite', action='store_true', default=False,
                        help='Force overwriting results')

    PARSER.add_argument('-l', '--lesion_mask', dest='lesion_mask', action='store', required=True, type=str,
                        help='Lesion mask in MNI sace')

    ARGS = PARSER.parse_args()

    extract_tractogram(lesion_mask=ARGS.lesion_mask, dependencies_directory=ARGS.dependencies_directory, output_folder=ARGS.output_folder, overwrite=ARGS.overwrite)

    
