import os
import glob 
import subprocess 

# path to your data directory in BIDS format
data_path = os.path.abspath('./data')

# pipeline name of the lesion segmentation folder inside derivativves
lesion_directory = 'lesion_mask'

# path to your dependencies directory
dependencies_directory = os.path.abspath('./dependencies/')

# patients list is extracted from BIDS structure
patients = [item for item in os.listdir(os.path.join(data_path, 'out')) if 'sub-' in item]

# iteration over all patients to analyse
for patient in patients : 
	print('Processing patient ' + patient)
	# timepoints list for each patient
	timepoints = [item for item in os.listdir(os.path.join(data_path, 'out' , patient)) if 'ses-' in item]

	# iteration over timepoints
	for timepoint in timepoints :

		# find the lesion mask
		lesion_mask = glob.glob(os.path.join(data_path, 'out/derivatives', lesion_directory, patient, timepoint, 'anat', '*mask.nii.gz'))

		# if only one lesion mask is found, we consider that one
		if len(lesion_mask) == 1 : 

			lesion_mask = lesion_mask[0]

		# otherwise something is wrong and the current timpoint for current patient is skipped
		else : 

			print('No lesion mask or multiple were found')
			continue

		# output folder were disconnectome output will be saved
		output_folder = os.path.join(data_path, 'out/derivatives/disconnectome/' ,patient , timepoint , 'anat')
		
		# STEP 1 :  extracting affected streamlines by overlapping lesion mask to tractography atlas and isolating streamlines passing
		# through the lesions
		print('Extracting Structural Disconnectome')
		subprocess.check_output('python ./extract_tractogram.py -d ' + dependencies_directory + ' -o '+output_folder+' -l '+ lesion_mask, shell=True)
		
		# STEP 2 :  create a disconnectome graph from connectivity matrix of affected streamlines and connectivity matrix of tractography 
		# atlas and extract topological features from it                     
		print('Modelling Brain Graph and extracting Topology')
		subprocess.check_output('python ./run_disconnectome.py -d ' + dependencies_directory + ' -o '+output_folder, shell=True)
		
		# STEP 3 :  create a Circos representation of disconnectivity within and between the main brain lobes
		print('Plotting Circos disconnectome')
		subprocess.check_output('Rscript ./plot_disconnectome.R -d ' + dependencies_directory + ' -o ' + output_folder, shell=True)