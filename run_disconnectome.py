#!/usr/bin/python3
#pylint: disable=C0301

"""
Script to create disconnectome graph from atlas connectivity and affected connectivity.
Topological features are also extracted.


Versions:
    Version 0.1: Base


"""
import subprocess
import os
import glob
import random
import argparse
import pandas as pd
import networkx as nx
import numpy as np
import scipy.io as sio
from scipy.sparse.csgraph import connected_components
from scipy.sparse import csr_matrix


__author__ = 'Veronica Lily Ravano'
__version__ = 0.1

class Disconnectome():
    """

    Attributes:
    
    output_folder :             output folder in BIDS structure where disconnectome outputs will be stored, and where the affected connectivity matrix
                                resulting from extract_tractogram.py is
    dependencies_directory :    dependencies directory where connectivity matrix of tractography atlas and MNI coordinates of the parcellationa tlas
                                are stored
    threshold :                 threshold used on edges weights to binarize the disconnectome graph
    overwrite :                 option to overwrite results if they already exist

    """

    def __init__(self, output_folder, dependencies_directory='./dependencies', threshold=0.5, overwrite = False ):
        """
        Returns a Disconnectome object that contains the paths of the directories and useful common variables

        """
        
        # Main directories
        
        # dependencies directory where atlas files are stored
        self.dependencies_directory = os.path.returnabspath(dependencies_directory)

        # output folder where connectivity matrix of affected tractogram is stored and where disconnectome features will be saved
        self.output_folder = output_folder

        # connectivity matrix of affected streamlines
        self.connectivity_affected = sio.loadmat(self.get_connectivity_affected())

        # connectivity matrix of the tractography atlas
        self.connectivity_atlas = sio.loadmat(self.get_connectivity_atlas())

        # overwrite option
        self.overwrite = overwrite

        # dataframe containing all regions in the brainnetome atlas, their label and the brain lobe to which they belong
        df = pd.read_csv(os.path.join(self.dependencies_directory,'coordinates_bn_mni.csv'))

        # ????
        df['index'] = df['index'] - 1

        # Brainnetome regions are sorted by their label
        df = df.sort_values('index')

        # brain region names sorted by their label
        self.atlas_labels = df['name']

        # brain region labels sorted
        self.atlas_index = df['index'].tolist()

        # CHECK here not necessary !!
        self.nodes = df.set_index('index')[['x.mni','y.mni','z.mni']].T.apply(tuple).to_dict()

        # threshold to be used for the binarization of the disconnectome graph. Must be between 0 and 1
        self.threshold = threshold
    
    def get_connectivity_atlas(self) :
        """
        Finds the connectivity matrix of the tractography atlas when the bain is parcelalted with the Brainnetom atlas.

        Returns : 
            path to the connectivity matrix of the tractography altas

        """
        return os.path.join(self.dependencies_directory,'whole_BN_connectivity_atlas.mat.BN_Atlas_274_combined.count.pass.connectivity.mat')

    def get_connectivity_affected(self) : 
        """
        TODO : ADD CHECK IF EXISTS

        Finds the connectivity matrix of the affected streamlines (those passing through the lesion mask).

        Returns : 
            path to the connectivity matrix of the affected streamlines

        """
        mat_list = glob.glob(os.path.join(self.output_folder,'*BN_Atlas*connectivity.mat'))

        if len(mat_list) == 1 : 
            return mat_list[0]

        else :
            print('Connectivity matrix not found')
            return 

    def get_edges(self, adj_mat):
        """
        Gets the edges from an adjacency matrix and creates a list of tuples where for each pair of regions, the weight of the edge is
        represented by the ratio of the corresponding value in the adjacency matrix with the maximum in the adjacency matrix

        Args : 
            adj_mat : a numpy array representing the connectivity matrix of brain areas corresponding to self.atlas_lables

        Returns : 
            a list where each element is one edge of the graph, represented as a tuple :
            (i,j, {'weight' : x}) is the representation of the edge connecting nodes i and j with a weight x 

        """
        return [(i,j,{'weight': adj_mat[i][j]}) #/ np.nanmax(adj_mat)})
                    for i in range(len(self.atlas_labels))
                    for j in range(len(self.atlas_labels))
                    if adj_mat[i][j]>0.0 ] 
    
    def connect_components(self, adjacency, new_file='None'):
        """
        For a given adjacency matrix, it finds the connected components and adds one edge per disconnected component to a randomly chosen
        node in the biggest connected component to end up with a connected graph. The new adjacency matrix is saved.

        Args :  adjacency : adjacency matrix as numpy array
                new_file : the path to the new adjacency matrix to save

        Returns : adjacency : the new connected adjacecny matrix

        """

        # read adjacency matrix
        graph = csr_matrix(adjacency)

        # find connected components and returns their labels
        n_components, labels = connected_components(csgraph=graph, directed=False, return_labels=True)

        # empty list of disconnected components
        disconnected_nodes = []
        # iteration over disconnected components
        for i in range(n_components) :

            if not i==0:
                # fill the list of disconnected nodes according to their label
                disconnected_nodes.append(next(j for j,obj in enumerate(labels) if obj==i))

        # nodes in the biggest components are always in the first connected component
        biggest_component_nodes = [i for i,node in enumerate(labels) if node==0]

        # a list of tuples representing new edges randomly chosen from nodes in the biggest connected component
        new_edges = [(item, random.choice(biggest_component_nodes)) for item in disconnected_nodes]

        # for each new edge
        for couple in new_edges : 
            # a small weight is added to the adjacency matrix at this position
            adjacency[couple[0]][couple[1]] = 0.01
        
        # if the new adjacency is to be saved
        if not new_file == 'None' :
            # adjacency is saved as new_file
            np.save(new_file, adjacency)
        
        return adjacency

    def generate_adjacency(self) :
        """
        Creates the disconnectome adjacency matrix by combining the affected connectivity matrix and the atlas connectivity matrix.
        Edges of the new adjacency matrix are weighted by the relative difference between atlas connectivity and affected connectivity,
        relative to the atlas connectivity : 
            disconnectome = ( atlas - affected ) / atlas
        The resulting adjacency matrix was checked for disconnected components and saved in the output_folder.


        Returns : 
            the path where the connectivity matrix was saved
            connectivity_disconnectome : the resulting adjacency matrix

        """

        # initialization of disconnectome connectivity matrix relative to atlas connectivity
        connectivity_disconnectome = 0*self.connectivity_affected['connectivity']

        # iteration over rows
        for i in range(self.connectivity_affected['connectivity'].shape[0]):

            # iteration over columns
            for j in range(self.connectivity_affected['connectivity'].shape[1]):

                #  to avoid division by 0
                if self.connectivity_atlas['connectivity'][i][j] != 0 :

                    # the edges weight is the relative difference of atlas connectivity and affected connectivity, relative to the atlas
                    connectivity_disconnectome[i][j] = (1 - self.connectivity_affected['connectivity'][i][j]/self.connectivity_atlas['connectivity'][i][j])
                else :

                    # to avoid division by 0
                    connectivity_disconnectome[i][j] = 0

        # final disconnectome adjacency matrix if verified for disconnected components, and resulting adjacency matrix is saved
        connectivity_disconnectome = self.connect_components(connectivity_disconnectome, os.path.join(self.output_folder, 'disconnectome_adjacency_matrix.npy'))

        return os.path.join(self.output_folder, 'disconnectome_adjacency_matrix.npy'), connectivity_disconnectome

    def define_brain_graph(self, connectivity_matrix) :
        """
        Defines brain graphs from a connectivity matrix using the Brainnetome atlas.
        Nodes aree added from the Brainnetome atlas (mind the order of nodes in the connectivity matrix) and 
        edges are added from the connectivity matrix

        Args : 
            connectivity_matrix :   npy array of size NxN with N being the number of nodes in the Brainnetome atlas
                                    and matrix values being the weights of graph edges. Careful, the nodes must be ordered according to 
                                    their label values in the Brainnetome !
        Returns :
            G : a NetworkX graph object
        """
        # initialize empty graph from NetworkX
        G = nx.Graph()

        # add nodes with positions defined as mni coordinates (for visualization purposes)
        G.add_nodes_from(self.nodes.keys())

        # add edges from the connectivity matrix
        G.add_edges_from(self.get_edges(connectivity_matrix))

        return G

    def extract_topology(self):
        """
        Computes topological features for the disconnectome graph using NetworkX
        Some features being defined for binary graphs only, a threshold on edges weight can be applied.

        """

        # generate the disconnectome adjacency matrix
        disconnectome_adjacency_path, connectivity_disconnectome = self.generate_adjacency()

        # new connectivity matrix
        connectivity_disconnectome_bin = connectivity_disconnectome

        # put edges with weight below the threshold to 0
        connectivity_disconnectome_bin[connectivity_disconnectome_bin<self.threshold] = 0 

        # connect components for the binary graph
        connectivity_disconnectome_bin = self.connect_components(connectivity_disconnectome_bin)

        # define disconnectome graph and binary disconnectome graph
        G_disconnectome, G_disconnectome_binary = self.define_brain_graph(connectivity_disconnectome), self.define_brain_graph(connectivity_disconnectome_bin)

        # Computation of Topological features
        # clustering coefficient computed as average over all nodes
        average_clustering = nx.average_clustering(G_disconnectome, weight='weight')
        
        # clustering coefficient per node
        clustering = nx.clustering(G_disconnectome, weight='weight')

        # average degree of neighborhood ogf each node
        average_neighbor_degree = nx.average_neighbor_degree(G_disconnectome, weight='weight')

        # strength per node (equivalend to degree but weighted)
        strength_sequence = [d for n, d in G_disconnectome.degree( weight='weight')]
        
        # average strength over all nodes
        average_strength = sum(strength_sequence)/len(strength_sequence)

        # betweenness centrality for each node
        betweenness_centrality = nx.betweenness_centrality(G_disconnectome, weight='weight')
        
        # transitivity - computed for binary graph
        transitivity = nx.transitivity(G_disconnectome_binary)

        # average shortest path length - computed for binary graph
        average_shortest_path_length = nx.average_shortest_path_length(G_disconnectome_binary) 

        # global efficiency - computed for binary graph
        global_efficiency = nx.global_efficiency(G_disconnectome_binary)

        # output_path for the topological features file
        output_path = os.path.join(self.output_folder, 'disconnectome_topological_features_' + str(self.threshold) + '.csv')

        # if it does not exist or overwrite
        if not os.path.isfile(output_path) or self.overwrite :

            # create a dataframe
            df = pd.DataFrame()

            # path to the adjacency matrix
            df.loc[0, 'adjacency'] = disconnectome_adjacency_path

            # large scale features
            df.loc[0, 'average_clustering'] = average_clustering
            df.loc[0, 'average_strength'] = average_strength
            df.loc[0, 'average_neighbor_degree'] = sum(average_neighbor_degree)/len(average_neighbor_degree) 
            df.loc[0, 'average_betweenness_centrality'] = sum(betweenness_centrality)/len(betweenness_centrality) 
            df.loc[0, 'average_shortest_path_length'] = average_shortest_path_length
            df.loc[0, 'transitivity' ] = transitivity
            df.loc[0, 'global_efficiency'] = global_efficiency

            # small scale features
            for idx,label in enumerate(self.atlas_labels):
                df.loc[0, label+'_strength'] = strength_sequence[idx]
                df.loc[0, label+'_neighbor_degree'] = average_neighbor_degree[idx]
                df.loc[0, label+'_clustering'] = clustering[idx]
                df.loc[0, label+'_betweenness_centrality'] = betweenness_centrality[idx]
                
            # save the topological dataframe
            df.to_csv(output_path, index=False)


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description="Extracting streamlines passing through lesions")

    PARSER.add_argument('-d', '--dependencies_directory', dest='dependencies_directory', action='store', required=False, type=str, default ='./dependencies',
                        help='Path to the dependencies directory. Default is ./dependencies')
    PARSER.add_argument('-t', '--threshold', dest='threshold', action='store', required=False, type=float, default =0.5,
                        help='Threshold value to be applied on edges weights to create the binary disconnectome graph. Default is 0.5')

    PARSER.add_argument('-o', '--output_directory', dest='output_folder', action='store', required=True, type=str,
                        help='Output folder of disconnectome')

    PARSER.add_argument('-f', '--force', dest='overwrite', action='store_true', default=False,
                        help='Force overwriting results')


    ARGS = PARSER.parse_args()

    # Create a Disconnectome object
    print('Building Brain Graph')
    DISC = Disconnectome(output_folder=ARGS.output_folder, dependencies_directory=ARGS.dependencies_directory, threshold = ARGS.threshold, overwrite=ARGS.overwrite)
    
    # Compute topology
    print('Extracting Topology')
    DISC.extract_topology()
